module.exports = {
    getPosts: (req, res) => {
        let query = "SELECT * FROM `posts` ORDER BY id ASC"; // query database to get all the posts

        // execute query
        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('selectposts.ejs', {
                title: "Welcome to our blog | View posts"
                ,posts: result
            });
        });
    },
};
