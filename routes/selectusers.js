module.exports = {
    getUsers: (req, res) => {
        let query = "SELECT * FROM `users` ORDER BY id ASC"; // query database to get all the users

        // execute query
        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('selectusers.ejs', {
                title: "Welcome to our blog | View users"
                ,users: result
            });
        });
    },
};
