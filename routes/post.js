const fs = require('fs');

module.exports = {
    addPostPage: (req, res) => {
        res.render('add-post.ejs', {
            title: "Welcome to our blog | Add a new post"
            ,message: ''
        });
    },
    addPost: (req, res) => {
        if (!req.files) {
            return res.status(400).send("No files were uploaded.");
        }

        let message = '';
        let title = req.body.title;
        let mensagem = req.body.mensagem;
        let tema = req.body.tema;
        let user_id = req.body.user_id;
        let key = req.body.key;
        let uploadedFile = req.files.image;
        let image_name = uploadedFile.name;
        let fileExtension = uploadedFile.mimetype.split('/')[1];
        image_name = key + '.' + fileExtension;

        let postnameQuery = "SELECT * FROM `posts` WHERE tag = '" + key + "'";

        db.query(postnameQuery, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            if (result.length > 0) {
                message = 'key already exists';
                res.render('add-post.ejs', {
                    message,
                    title: "Welcome to our blog | Add a new post"
                });
            } else {
                // check the filetype before uploading it
                if (uploadedFile.mimetype === 'image/png' || uploadedFile.mimetype === 'image/jpeg' || uploadedFile.mimetype === 'image/gif') {
                    // upload the file to the /public/assets/img directory
                    uploadedFile.mv(`public/assets/img/${image_name}`, (err ) => {
                        if (err) {
                            return res.status(500).send(err);
                        }
                        // send the post's details to the database
                        let query = "INSERT INTO `posts` (title, mensagem, tema, user_id, image, tag) VALUES ('" +
                        title + "', '" + mensagem + "', '" + tema + "', '" + user_id + "', '" + image_name + "', '" + key + "')";
                        db.query(query, (err, result) => {
                            if (err) {
                                return res.status(500).send(err);
                            }
                            res.redirect('/');
                        });
                    });
                } else {
                    message = "Invalid File format. Only 'gif', 'jpeg' and 'png' images are allowed.";
                    res.render('add-post.ejs', {
                        message,
                        title: "Welcome to our blog | Add a new post"
                    });
                }
            }
        });
    },
    editPostPage: (req, res) => {
        let postId = req.params.id;
        let query = "SELECT * FROM `posts` WHERE id = '" + postId + "' ";
        db.query(query, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.render('edit-post.ejs', {
                title: "Edit  Post"
                ,post: result[0]
                ,message: ''
            });
        });
    },
    editPost: (req, res) => {
        let postId = req.params.id;
        let title = req.body.title;
        let mensagem = req.body.mensagem;
        let tema = req.body.tema;
        let user_id = req.body.user_id;

        let query = "UPDATE `posts` SET `title` = '" + title + "', `mensagem` = '" + mensagem + "', `tema` = '" + tema + "', `user_id` = '" + user_id + "' WHERE `posts`.`id` = '" + postId + "'";
        db.query(query, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.redirect('/');
        });
    },
    deletePost: (req, res) => {
        let postId = req.params.id;
        let getImageQuery = 'SELECT image from `posts` WHERE id = "' + postId + '"';
        let deletePostQuery = 'DELETE FROM posts WHERE id = "' + postId + '"';

        db.query(getImageQuery, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }

            let image = result[0].image;

            fs.unlink(`public/assets/img/${image}`, (err) => {
                if (err) {
                    return res.status(500).send(err);
                }
                db.query(deletePostQuery, (err, result) => {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    res.redirect('/');
                });
            });
        });
    }
};
