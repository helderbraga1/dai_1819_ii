const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const path = require('path');
const app = express();
var mongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://localhost:27017/";

const fs = require('fs');

const {getHomePage} = require('./routes/index');
const {getUsers} = require('./routes/selectusers');
const {getPosts} = require('./routes/selectposts');
const {addUserPage, addUser, deleteUser, editUser, editUserPage} = require('./routes/user');
const {addPostPage, addPost, deletePost, editPost, editPostPage} = require('./routes/post');
const port = 5000;

mongoClient.connect(mongoUrl, function (error, database) {
    if (error) throw error;
    var databasePointer = database.db("blogdb");
    var objectQuery = { firstname: "Test123", lastname: "321tseT", position: "1", number:"3", image:"01.png",user_name:"teste1234"};
    databasePointer.collection("users").insertOne(objectQuery, function (error, result) {
        if (error) throw error;
        console.log("1 post inserted");
        database.close();

    });

});

// create connection to database
// the mysql.createConnection function takes in a configuration object which contains host, user, password and the database name.
const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'blogdb'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
app.set('view engine', 'ejs'); // configure template engine
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client
app.use(express.static(path.join(__dirname, 'public'))); // configure express to use public folder
app.use(fileUpload()); // configure fileupload

// routes for the app

app.get('/', getHomePage);
app.get('/selectusers', getUsers);
app.get('/addU', addUserPage);
app.get('/edit/:id', editUserPage);
app.get('/delete/:id', deleteUser);
app.post('/addU', addUser);
app.post('/edit/:id', editUser);

// Posts
app.get('/selectposts', getPosts);
app.get('/addP', addPostPage);
app.get('/editP/:id', editPostPage);
app.get('/deleteP/:id', deletePost);
app.post('/addP', addPost);
app.post('/editP/:id', editPost);


// set the app to listen on the port
app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
});